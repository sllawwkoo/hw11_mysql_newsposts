const { useKnex } = require("./base");
const { cliOptions } = require("./cliOptions");

const getAll = async () => {
	try {
		const { knex } = await useKnex();
		const result = await knex("newsPosts").select('*');

		return result;
	} catch (err) {
		console.log(err);
	}
};

(async () => {
	console.table(await getAll());
})();
