const { useKnex } = require("./base");
const { cliOptions } = require("./cliOptions");

const update = async () => {
	try {
		const { knex } = await useKnex();

		const data = {};

		for (let key in cliOptions) {
			if (key !== "id") {
				data[key] = cliOptions[key];
			}
		}

		data["updated_at"] = new Date();

		await knex("newsPosts").where("id", cliOptions.id).update(data);

		console.log("Update complete!");
	} catch (err) {
		console.log(err);
	}
};

(async () => {
	console.log(await update());
})();
