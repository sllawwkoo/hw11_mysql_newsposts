const { useKnex } = require("./base");
const {cliOptions} = require("./cliOptions");

const insert = async () => {
	try {
		const { knex } = await useKnex();
		await knex("newsPosts").insert({
			title: cliOptions.title,
			text: cliOptions.text,
			created_at: new Date(),
			updated_at: new Date(),
		});
		console.log('Insert complete!');
	} catch (err) {
		console.log(err);
	}
};

(async () => {
	console.log(await insert());
})();
