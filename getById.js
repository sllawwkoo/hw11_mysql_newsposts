const { useKnex } = require("./base");
const { cliOptions } = require("./cliOptions");

const getById = async () => {
	try {
		const { knex } = await useKnex();

		const record = await knex("newsPosts").where("id", cliOptions.id).first();

		if (record) {
			console.log(record);
		} else {
			console.log(`Record with id ${cliOptions.id} does not exist.`);
		}
	} catch (err) {
		console.log(err);
	}
};

(async () => {
	console.log(await getById());
})();
