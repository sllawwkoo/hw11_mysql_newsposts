const { useKnex } = require("./base");
const { cliOptions } = require("./cliOptions");

const deleteById = async () => {
	try {
		const { knex } = await useKnex();

		const deletedCount = await knex("newsPosts")
			.where("id", cliOptions.id)
			.del();

		if (deletedCount > 0) {
			console.log(`Record with id ${cliOptions.id} has been deleted.`);
		} else {
			console.log(`Record with id ${cliOptions.id} does not exist.`);
		}
	} catch (err) {
		console.log(err);
	}
};

(async () => {
	await deleteById();
})();
