const useKnex = async () => {
	const knex = require("knex")({
		client: "mysql2",
		// debug: true,
		connection: {
			host: "127.0.0.1",
			user: "root",
			password: "0000",
			port: 3306,
		},
	});

	const init = async () => {
		await knex.raw("CREATE DATABASE IF NOT EXISTS danitDB");
		await knex.raw("USE danitDB");

		//delete all tables
		// await knex.raw('DROP TABLE IF EXISTS newsPosts');
	};

	await init();

	return {
		knex,
	};
};


module.exports = {
	useKnex,
};
