const comandLineArgs = require("command-line-args");

const optionsDefinitions = [
	{ name: "id", type: Number }, 
	{ name: "title", type: String },
	{ name: "text", type: String },
];

const cliOptions = comandLineArgs(optionsDefinitions);

module.exports = {
	cliOptions,
};
