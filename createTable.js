const { useKnex } = require("./base");
const createTable = async () => {
	return new Promise(async (resolve, reject) => {
		const { knex } = await useKnex();

		knex.schema
			.createTable("newsPosts", (table) => {
				table.increments("id").primary();
				table.string("title");
				table.string("text");
				table.timestamps("created_at");
				
				resolve("Table created!");
			}).catch((err) => {
				reject(err);
			});
	});
};

(async () => {
	console.log(await createTable());
})()
